require 'active_record'

module GitLab
  module Repack
    autoload :Config, 'gitlab_repack/config'
    autoload :Shell, 'gitlab_repack/shell'
    autoload :Estimate, 'gitlab_repack/estimate'
    autoload :Repack, 'gitlab_repack/repack'
    autoload :BloatAnalyzer, 'gitlab_repack/bloat_analyzer'
    autoload :Database, 'gitlab_repack/database'
    autoload :Grafana, 'gitlab_repack/grafana'
    autoload :Logging, 'gitlab_repack/logging'

    DEFAULT_CONFIG_FILE = File.join(__dir__, '..',
                                    'config', 'gitlab-repack.yml')

    def self.init!(config_file = DEFAULT_CONFIG_FILE)
      Config.instance.read_from(config_file)

      GitLab::Repack::Config.instance.database.tap do |db|
        ActiveRecord::Base.establish_connection(db)
      end

      Database.init!
    end
  end
end
