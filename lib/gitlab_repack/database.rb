module GitLab
  module Repack
    module Database
      extend Logging
      EXTENSION_QUERY = "SELECT count(*) FROM pg_extension WHERE extname = '%<extname>s'".freeze

      def execute_query(query)
        log.info(query) unless query.start_with?('SELECT')

        connection.execute(query)
      end

      def connection
        ActiveRecord::Base.connection
      end

      def init!
        if extension_already_installed?('pg_repack')
          log.info("The 'pg_repack' extension is already installed")
        else
          log.info("The 'pg_repack' extension is not installed")
          install_pg_repack_extension
        end
      end

      def extension_already_installed?(extname)
        query = format(EXTENSION_QUERY, extname: extname)
        result = execute_query(query)
        result.to_a.first['count'] == 1
      end

      def install_pg_repack_extension
        log.info("Installing the 'pg_repack' extension")
        execute_query('CREATE EXTENSION IF NOT EXISTS pg_repack')
        execute_query('SET statement_timeout=0')
        execute_query('SET idle_in_transaction_session_timeout=0')
      end

      module_function :execute_query, :connection, :init!
      module_function :extension_already_installed?, :install_pg_repack_extension
    end
  end
end
