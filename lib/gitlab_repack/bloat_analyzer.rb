require 'ostruct'

module GitLab
  module Repack
    class BloatAnalyzer
      def estimate_bloat(analyze: false)
        analyze! if analyze

        {
          tables: execute_estimation(:table),
          btrees: execute_estimation(:btree)
        }
      end

      private

      def analyze!
        execute('ANALYZE')
      end

      def execute_estimation(type)
        query = File.read(File.join(__dir__, "bloat_#{type}.sql"))
        execute(query).to_a.map do |record|
          OpenStruct.new(record)
        end
      end

      def execute(query)
        ActiveRecord::Base.connection.execute(query)
      end
    end
  end
end
