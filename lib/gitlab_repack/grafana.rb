require 'httparty'
require 'date'
require 'json'
require 'pry'

module GitLab
  module Repack
    class Grafana
      attr_reader :auth_key, :base_url

      def initialize(auth_key: nil, base_url: 'https://dashboards.gitlab.net')
        @auth_key = auth_key || Config.instance.grafana.auth_key
        @base_url = base_url || Config.instance.grafana.base_url
      end

      def create_annotation(object_name)
        annotation = {
          time: DateTime.now.strftime('%Q').to_i,
          tags: ['pgrepack', Config.instance.general.env],
          text: "pg_repack: #{object_name}",
          isRegion: true
        }

        id = post(annotation) if enabled?

        GrafanaAnnotation.new(self, id, object_name)
      end

      def notify_end(annotation_id, object_name)
        return unless enabled?

        payload = {
          time: DateTime.now.strftime('%Q').to_i,
          text: "pg_repack: #{object_name} (done)"
        }
        update(annotation_id, payload)
      rescue StandardError => e
        puts "Failed to update Grafana annotation due to: #{e}"
      end

      def enabled?
        auth_key && base_url
      end

      private

      def headers
        { 'Authorization' => "Bearer #{auth_key}", 'Content-Type' => 'application/json' }
      end

      def post(annotation)
        result = HTTParty.post(endpoint, body: annotation.to_json, headers: headers)

        result['id']
      rescue StandardError => e
        puts "Failed to create Grafana annotation due to: #{e}"
        nil
      end

      def update(annotation_id, payload)
        all_headers = headers.merge('Content-Type' => 'application/json')

        HTTParty.patch("#{endpoint}/#{annotation_id}", body: payload.to_json, headers: all_headers)
      end

      def endpoint
        "#{base_url}/api/annotations"
      end
    end
  end

  class GrafanaAnnotation
    attr_reader :grafana, :annotation_id, :object_name

    def initialize(grafana, annotation_id, object_name)
      @grafana = grafana
      @annotation_id = annotation_id
      @object_name = object_name
    end

    def notify_end!
      return unless annotation_id

      grafana.notify_end(annotation_id, object_name)
    end
  end
end
