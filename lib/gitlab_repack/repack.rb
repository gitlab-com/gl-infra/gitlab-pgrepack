require 'pry'
require 'open3'

module GitLab
  module Repack
    class RepackFailed < StandardError; end

    class Repack
      include Logging

      attr_reader :grafana

      def initialize(grafana: Grafana.new)
        @grafana = grafana
      end

      def execute(type, objects)
        objects.each do |object|
          annotation = grafana.create_annotation(object)
          begin
            log.info("Starting repack for #{object}")
            class_for(type).new(object).repack!
          ensure
            annotation.notify_end!
          end
        end
      end

      private

      def class_for(type)
        case type
        when :btrees
          IndexRepacking
        when :tables
          TableRepacking
        else
          raise "Unknown type: #{type}"
        end
      end
    end

    class OidResolver
      include Database

      def resolve(schema, object, kind)
        query = <<~SQL
          SELECT pg_class.oid
          FROM pg_class
          JOIN pg_namespace ON pg_class.relnamespace=pg_namespace.oid
          WHERE pg_namespace.nspname = '#{schema}'
          AND pg_class.relkind='#{kind}'
          AND pg_class.relname='#{object}'
        SQL
        result = execute_query(query)

        raise "Unexpectedly, we did not find an oid for '#{object}'" unless result&.first

        Integer(result.first['oid'])
      end
    end

    class Repacking
      include Logging

      attr_reader :object_name, :oid_resolver

      def initialize(object_name, oid_resolver: OidResolver.new)
        raise 'No object name provided' unless object_name && !object_name.strip.empty?

        @object_name = object_name
        @oid_resolver = oid_resolver
      end

      def repack!
        # Ensure any leftovers are removed
        cleanup!

        # Run any before hooks
        before

        Signal.trap('TERM') do
          Process.kill('TERM', @current_repack_pid) if @current_repack_pid
        end

        # execute actual re-packing
        execute!
      ensure
        begin
          # We're also cleaning up here since we're good citizens
          cleanup!
        rescue StandardError => e
          log.warn("Nested exception: #{e}")
        end

        begin
          after
        rescue StandardError => e
          log.warn("Nested exception: #{e}")
        end
      end

      def execute!
        Open3.popen3(command) do |_, stdout, stderr, wait_thr|
          @current_repack_pid = wait_thr.pid

          Thread.new { consume_io(stdout) }
          Thread.new { consume_io(stderr) }

          raise RepackFailed, 'process returned non-zero exit code.' unless wait_thr.value.success?
        end
      ensure
        @current_repack_pid = nil
      end

      private

      def consume_io(io)
        buf = []
        while (line = io.gets)
          log.info(line.strip)
          buf << line
        end
        buf.join
      rescue StandardError
        # we ignore these
      end

      def before
        # implemented in subclass (optional)
      end

      def after
        # implemented in subclass (optional)
      end

      def schema
        path = object_name.split('.')
        raise "Object is not schema qualified: #{object_name}" unless path.size == 2

        path.first
      end

      def relname
        object_name.split('.').last
      end
    end

    class TableRepacking < Repacking
      include Database

      private

      def before
        toggle_autovacuum(enabled: false)
      end

      def after
        toggle_autovacuum(enabled: true)
      end

      def toggle_autovacuum(enabled: true)
        state = enabled ? 'on' : 'off'
        execute_query("ALTER TABLE #{object_name} SET (autovacuum_enabled = #{state})")
      end

      def cleanup!
        # Here, we may need to cleanup log tables, a type and a trigger
        # Those can be left around from previous (failed) pg_repack invocations

        original_oid = oid_resolver.resolve(schema, relname, 'r')

        log_table = "log_#{original_oid}"
        new_table = "table_#{original_oid}"
        type = "pk_#{original_oid}"

        execute_query("DROP TRIGGER IF EXISTS repack_trigger ON \"#{schema}\".\"#{relname}\" RESTRICT")
        execute_query("DROP TABLE IF EXISTS repack.\"#{log_table}\" RESTRICT")
        execute_query("DROP TABLE IF EXISTS repack.\"#{new_table}\" RESTRICT")
        execute_query("DROP TYPE IF EXISTS repack.\"#{type}\" RESTRICT")
      end

      def command
        "#{Config.instance.repack.command} --table=#{object_name}"
      end
    end

    class IndexRepacking < Repacking
      include Database

      private

      def cleanup!
        # Here, we may need to cleanup INVALID indexes
        # Those are created in the same schema and with `index_$oid` name
        # ($oid is the object-identifier of the original index)

        original_oid = oid_resolver.resolve(schema, relname, 'i')

        temporary_index_name = "index_#{original_oid}"

        execute_query("DROP INDEX CONCURRENTLY IF EXISTS \"#{schema}\".\"#{temporary_index_name}\" RESTRICT")
      end

      def command
        "#{Config.instance.repack.command} --index=#{object_name}"
      end
    end
  end
end
