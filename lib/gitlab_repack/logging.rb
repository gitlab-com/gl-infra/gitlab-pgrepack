require 'logger'

module GitLab
  module Repack
    module Logging
      module_function

      def log
        @log ||= Logger.new($stdout)
      end
    end
  end
end
