require 'yaml'
require 'ostruct'

module GitLab
  module Repack
    class Config
      include Singleton

      def read_from(file)
        raise 'no file provided' unless file

        @config = YAML.load_file(file)
      end

      def database
        @config && @config['database']
      end

      def estimate
        @config && OpenStruct.new(@config['estimate'])
      end

      def repack
        @config && OpenStruct.new(@config['repack'])
      end

      def general
        @config && OpenStruct.new(@config['general'])
      end

      def grafana
        @config && OpenStruct.new(@config['grafana'])
      end
    end
  end
end
