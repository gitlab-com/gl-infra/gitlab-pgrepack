require 'filesize'
require 'active_support/core_ext/numeric/bytes'

module GitLab
  module Repack
    class Estimate
      attr_reader :bloat_analyzer

      REPACK_THRESHOLD = 30 # in percent
      REPACK_MIN_SIZE = 100.megabytes # minimum real size of index/table for repacking
      OBJECTS_PER_REPACK = 5

      def initialize(bloat_analyzer = BloatAnalyzer.new)
        @bloat_analyzer = bloat_analyzer
      end

      def execute(type: :tables, printable_name: 'Table', objects_per_repack: Config.instance.estimate.objects_per_repack)
        objects_per_repack ||= OBJECTS_PER_REPACK

        stats = bloat_analyzer.estimate_bloat

        puts "#{printable_name.upcase} bloat:"
        puts printable_summary(stats[type])

        puts "Recommended repacking for #{printable_name} bloat:"
        if (recommendation = recommend(stats[type]))
          recommendation.each_slice(objects_per_repack).each do |objects|
            object_list = objects.map { |e| "#{e.schemaname}.#{e.object_name}" }.join(',')
            puts "gitlab-pgrepack repack --type=#{type} --objects=#{object_list}"
          end

          puts "Expected cleanup of #{bytes(recommendation.map(&:bloat_size).reduce(:+))} of bloat."
        else
          puts 'Nothing to do.'
        end
        puts
      end

      private

      def recommend(stats)
        estimate_config = GitLab::Repack::Config.instance.estimate

        ratio_threshold = estimate_config.ratio_threshold || REPACK_THRESHOLD
        real_size_threshold = estimate_config.real_size_threshold || REPACK_MIN_SIZE

        sorted_objects = stats.sort_by do |e|
          -e.bloat_size
        end

        objects = sorted_objects.select do |e|
          e.bloat_pct.to_i > ratio_threshold && \
            e.real_size > real_size_threshold
        end

        return nil if objects.empty?

        objects
      end

      def printable_summary(stats)
        summary = summarize(stats)
        "#{bytes(summary[:bloat_size])} of bloat " \
          "(#{percentage(summary[:bloat_pct])} % ratio of total size)"
      end

      def bytes(value)
        return nil if value.nil?

        Filesize.from("#{value} B").pretty
      end

      def percentage(value)
        return nil if value.nil?

        (value.to_f * 100).round(1)
      end

      def summarize(stats)
        {
          bloat_size: sum(stats, :bloat_size),
          real_size: sum(stats, :real_size)
        }.tap do |h|
          h[:bloat_pct] = division(h[:bloat_size], h[:real_size])
        end
      end

      def division(dividend, divisor)
        return nil unless divisor.to_i.positive?

        dividend / divisor
      end

      def sum(data, field)
        data.reduce(0) do |sum, e|
          puts "sum: #{sum.inspect}, e[#{field}]: #{e[field].inspect}"
          sum + e[field]
        end
      end
    end
  end
end
