require 'thor'
require 'pry'

GitLab::Repack.init!

module GitLab
  module Repack
    class Shell < Thor
      package_name 'gitlab-repack'

      desc 'estimate', 'Estimate database bloat and provide a repacking recommendation.'
      def estimate
        puts <<~NOTE
          Note: This is based on an estimation of database bloat.
          Note: Executing full table repacking also removes index bloat.

        NOTE

        Estimate.new.tap do |task|
          task.execute(type: :tables, printable_name: 'table')
          task.execute(type: :btrees, printable_name: 'index')
        end
      end

      desc 'repack', 'Execute database repacking to reduce bloat levels.'
      method_option :type, required: true, enum: %w[tables btrees]
      method_option :objects, required: true, type: :string
      def repack
        Repack.new.execute(options[:type].to_sym, options[:objects].split(','))
      end

      desc 'console', 'Start interactive console'
      def console
        binding.pry # rubocop:disable Lint/Debugger
      end
    end
  end
end
