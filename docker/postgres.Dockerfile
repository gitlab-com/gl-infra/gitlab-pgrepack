FROM postgres:12.11

RUN apt update --fix-missing \
    && \
    apt install --assume-yes curl lsb-release \
    && \
    lsb_release -cs \
    && \
    curl https://www.postgresql.org/media/keys/ACCC4CF8.asc | apt-key add - \
    && \
    echo "deb http://apt.postgresql.org/pub/repos/apt/ $(lsb_release -cs)-pgdg main" > /etc/apt/sources.list.d/pgdg.list \
    && \
    apt update --fix-missing \
    && \
    apt install --assume-yes postgresql-12-repack \
    && \
    apt clean \
    && \
    mkdir -p /docker-entrypoint-initdb.d
