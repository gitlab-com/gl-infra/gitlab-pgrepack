require 'spec_helper'

require 'gitlab_repack/repack'

describe GitLab::Repack::TableRepacking do
  describe '#repack!' do
    subject { described_class.new('foo.bar', oid_resolver: oid_resolver).repack! }

    let(:oid_resolver) { double('resolver', resolve: nil) }
    let(:oid) { 42 }
    let(:conn) { ActiveRecord::Base.connection }
    let(:success) { ['', '', double('status', success?: true)] }

    before do
      allow(oid_resolver).to receive(:resolve).with('foo', 'bar', 'r').and_return(oid)
      allow(Open3).to receive(:popen3).and_return(success) # do nothing
    end

    it 'drop the log and temporary table, drops type and removes trigger' do
      # This spec is more tight with regard to not only expecting queries
      # but also making sure we don't have any unexpected queries (other than ALTER TABLE)
      allow(conn).to receive(:execute).with(/ALTER TABLE/)

      expect(conn).to receive(:execute).twice.with("DROP TABLE IF EXISTS repack.\"log_#{oid}\" RESTRICT")
      expect(conn).to receive(:execute).twice.with("DROP TABLE IF EXISTS repack.\"table_#{oid}\" RESTRICT")
      expect(conn).to receive(:execute).twice.with("DROP TYPE IF EXISTS repack.\"pk_#{oid}\" RESTRICT")
      expect(conn).to receive(:execute).twice.with('DROP TRIGGER IF EXISTS repack_trigger ON "foo"."bar" RESTRICT')

      subject
    end

    context 'not caring about cleanup' do
      before do
        allow(conn).to receive(:execute)
      end

      it 'disables autovaccuum before executing repack' do
        expect(conn).to receive(:execute).with('ALTER TABLE foo.bar SET (autovacuum_enabled = off)')

        subject
      end

      it 're-enables autovaccuum after running repack' do
        expect(conn).to receive(:execute).with('ALTER TABLE foo.bar SET (autovacuum_enabled = on)')

        subject
      end

      it 'executes pg_repack on the table' do
        expect(Open3).to receive(:popen3) do |command|
          expect(command).to match(/pg_repack .* --table=foo\.bar/)
        end.and_return(success)

        subject
      end
    end
  end
end

describe GitLab::Repack::IndexRepacking do
  describe '#repack!' do
    subject { described_class.new('foo.bar', oid_resolver: oid_resolver).repack! }

    let(:oid_resolver) { double('resolver', resolve: nil) }
    let(:oid) { 42 }
    let(:conn) { ActiveRecord::Base.connection }
    let(:success) { ['', '', double('status', success?: true)] }

    before do
      allow(oid_resolver).to receive(:resolve).with('foo', 'bar', 'i').and_return(oid)
    end

    it 'drops the temporary index' do
      allow(Open3).to receive(:popen3).and_return(success) # do nothing

      expect(conn).to receive(:execute).twice.with("DROP INDEX CONCURRENTLY IF EXISTS \"foo\".\"index_#{oid}\" RESTRICT")

      subject
    end

    it 'executes pg_repack on the table' do
      expect(Open3).to receive(:popen3) do |command|
        expect(command).to match(/pg_repack .* --index=foo\.bar/)
      end.and_return(success)

      subject
    end
  end
end
