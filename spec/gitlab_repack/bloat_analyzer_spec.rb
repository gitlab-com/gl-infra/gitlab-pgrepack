require 'spec_helper'

describe GitLab::Repack::BloatAnalyzer do
  describe '#estimate_bloat' do
    subject { described_class.new.estimate_bloat(analyze: analyze) }

    context 'with analyze enabled' do
      let(:analyze) { true }

      it 'runs full database analyze' do
        expect(ActiveRecord::Base.connection).to receive(:execute).with('ANALYZE')
        allow(ActiveRecord::Base.connection).to receive(:execute).and_call_original

        subject
      end
    end

    context 'without analyze' do
      let(:analyze) { false }

      it 'runs full database analyze' do
        expect(ActiveRecord::Base.connection).not_to receive(:execute).with('ANALYZE')

        subject
      end
    end
  end
end
