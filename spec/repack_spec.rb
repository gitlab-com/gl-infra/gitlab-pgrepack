require 'spec_helper'
require 'open3'

describe 'repack task' do
  subject { Open3.capture3('bin/gitlab-pgrepack repack --type=btrees --objects=public.foo_pkey') }

  class PgTable < ActiveRecord::Base
  end

  # TODO: Use module
  let(:conn) { ActiveRecord::Base.connection }

  before do
    if PgTable.where(schemaname: 'public', tablename: 'foo').exists?
      conn.execute('DROP TABLE public.foo')
    end

    conn.execute('CREATE TABLE public.foo (id serial primary key, created_at timestamptz default now(), bar integer)')

    10.times do
      conn.execute('INSERT INTO public.foo (bar) SELECT t FROM generate_series(1, 100000) t')
      3.times do
        conn.execute('DELETE FROM public.foo WHERE random() < 0.1')
      end
    end

    conn.execute('ANALYZE public.foo')
  end

  after do
    conn.execute('DROP TABLE public.foo')
  end

  it 'does not fail' do
    stdout, stderr, status = subject

    unless status.success?
      # Useful for debugging
      puts "STDOUT: #{stdout}"
      puts "STDERR: #{stderr}"
    end

    expect(status).to be_success
  end

  it 'changes the size of the index' do
    expect { subject }.to change { conn.execute("select pg_relation_size('public.foo_pkey') as index_size").first['index_size'] }
  end
end
