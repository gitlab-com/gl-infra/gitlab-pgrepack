require 'spec_helper'
require 'open3'

describe 'repack: estimate' do
  subject { Open3.capture3('bin/gitlab-pgrepack estimate') }

  class PgTable < ActiveRecord::Base
  end

  shared_examples_for 'integration test' do
    it 'does not fail' do
      _, _, status = subject

      expect(status).to be_success
    end

    it 'provides a table bloat estimate' do
      stdout, = subject

      expect(stdout).to include('TABLE bloat:')
    end

    it 'provides an index bloat estimate' do
      stdout, = subject

      expect(stdout).to include('INDEX bloat:')
    end

    it 'provides a table bloat maintenance recommendation' do
      stdout, = subject

      expect(stdout).to include('Recommended repacking for table bloat')
    end

    it 'provides an index bloat maintenance recommendation' do
      stdout, = subject

      expect(stdout).to include('Recommended repacking for index bloat')
    end

    it 'does not write to sderr' do
      _, stderr, = subject

      expect(stderr).to be_empty
    end
  end

  context 'with an empty database' do
    it_behaves_like 'integration test'
  end

  context 'with a bloated table' do
    let(:conn) { ActiveRecord::Base.connection }

    before do
      if PgTable.where(schemaname: 'public', tablename: 'foo').exists?
        conn.execute('DROP TABLE public.foo')
      end

      conn.execute('CREATE TABLE public.foo (id serial primary key, created_at timestamptz default now(), bar bigint, baz bigint, qux bigint, quux timestamptz default now())')
      conn.execute('ALTER TABLE public.foo SET (autovacuum_enabled = off, fillfactor = 50)')

      10.times do
        conn.execute('INSERT INTO public.foo (bar,baz,qux) SELECT t, 0, 1  FROM generate_series(1, 100000) t')
        5.times do
          conn.execute('DELETE FROM public.foo WHERE random() < 0.1')
        end
      end

      conn.execute('ANALYZE public.foo')
    end

    after do
      conn.execute('DROP TABLE public.foo')
    end

    it_behaves_like 'integration test'

    it 'recommends table bloat cleanup' do
      stdout, = subject

      expect(stdout).to match(/gitlab-pgrepack repack --type=tables --objects=public\.foo/)
    end
  end
end
