# gitlab-pgrepack

`gitlab-pgrepack` is a ruby command line tool to clean up database bloat
using [pg_repack](https://github.com/reorg/pg_repack).

It provides an interface to pg_repack to execute bloat maintenance and
adds robust cleanup and other automation around it.

Also, `gitlab-pgrepack` provides bloat estimation and recommends
which tables or indexes need repacking (along with appropriate
commands).

Additionally, annotations can be pushed to a Grafana instance in order
to mark start and end of repacking actions.

## Installation

In order to execute the tool, a ruby 2.5 environment with bundler is
needed. We recommend using [rvm.io](http://rvm.io/) to install ruby.

* Checkout git repo locally
* `gem install bundler`
* `bundle install`

After that, `bin/gitlab-pgrepack` should work as expected and yield a
CLI documentation.

In order to execute repacking in-database, the [`pg_repack` extension](https://github.com/reorg/pg_repack) is
required to be available to the PostgreSQL database. The `pg_repack`
command line tool needs to be available local to `gitlab-pgrepack` too.

## Execution

```
gitlab-repack commands:
  gitlab-pgrepack console                               # Start interactive console
  gitlab-pgrepack estimate                              # Estimate database bloat and provide a repacking recommendation.
  gitlab-pgrepack help [COMMAND]                        # Describe available commands or one specific command
  gitlab-pgrepack repack --objects=OBJECTS --type=TYPE  # Execute database repacking to reduce bloat levels.
```

A typical invocation of `estimate` looks like this:

```
Note: This is based on an estimation of database bloat. Note that
executing full table repacking also removes index bloat.

TABLE bloat:
43.02 GiB of bloat (2.3 % ratio of total size)
Recommended repacking for table bloat:
Nothing to do.

INDEX bloat:
484.94 GiB of bloat (52.8 % ratio of total size)
Recommended repacking for index bloat:
gitlab-pgrepack repack --type=btrees --objects=public.some_index
```

Note this is based on bloat estimation (from [ioguix/pgsql-bloat-estimation](https://github.com/ioguix/pgsql-bloat-estimation)).
Expect this to give a rough idea of the bloat prevalent in the database. It is not an exact measurement and does not have any bound on the error.


Above we can see that index maintenance is suggested for
`public.some_index`. This can be executed with `gitlab-pgrepack repack --type=btrees --objects=public.some_index`.

The `repack` task takes care of repacking the given table(s) or
index(es). It also makes sure to clean up any lingering temporary
objects (from pg_repack) before and after the repacking, also on
failures.

Repacking takes the following options:

```
Usage:
  gitlab-pgrepack repack --objects=OBJECTS --type=TYPE

Options:
  --type=TYPE         # Possible values: tables, btrees
  --objects=OBJECTS   # comma-separated list of object names with schema, e.g. public.foo,public.bar
```

For tables, `repack` automatically disables autovacuum during repacking.
It is re-enabled once repacking finished (or failed).

### Production considerations

Note it is advisable to disable `idle_in_transaction_session_timeout`
for the user pg_repack is being run with.

At all times, it is recommended to watch out for lock increases and
cancel in-progress repacking as needed. Locks may accumulate and bring
down the site if no manual action is being taken.

## Docker

We use a custom docker image for postgres with pg_repack installed.

Build and push it locally with:

```
docker build --file=docker/postgres.Dockerfile --tag registry.gitlab.com/gitlab-com/gl-infra/gitlab-pgrepack/postgres:12.11 .
docker push registry.gitlab.com/gitlab-com/gl-infra/gitlab-pgrepack/postgres:12.11
```

```
docker build --file=docker/ruby.Dockerfile --tag registry.gitlab.com/gitlab-com/gl-infra/gitlab-pgrepack/ruby:3.1.2 .
docker push registry.gitlab.com/gitlab-com/gl-infra/gitlab-pgrepack/ruby:3.1.2
```

## Configuration

Typical configuration along with comments can be found in [config/gitlab-repack.yml](config/gitlab-repack.yml).

## Testing

For rspec testing, we need a postgres container with pg_repack installed. There's an image available in the project's registry for this: `registry.gitlab.com/gitlab-com/gl-infra/gitlab-pgrepack/postgres:9.6`. It can also be built from `docker/Dockerfile`.

### Local testing

* Change `config/gitlab-repack.yml` to point to `localhost` for
  `repack.command` and `database.host`

```
docker-compose up
bundle exec rspec spec
```
