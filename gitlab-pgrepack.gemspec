lib = File.expand_path("../lib", __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require "gitlab_repack/version"

Gem::Specification.new do |s|
  s.name          = "gitlab-repack"
  s.version       = GitLab::Repack::VERSION
  s.summary       = "GitLab tool to interface with pg_repack"
  s.description   = "GitLab tool to interface with pg_repack to reduce database bloat levels."
  s.authors       = ["Andreas Brandl"]
  s.email         = "abrandl@gitlab.com"

  s.files = Dir.glob([
    'lib/**/*.rb',
    'lib/**/*.sql',
    'README.md',
    'LICENSE',
    '*.gemspec'
  ]).select { |path| File.file?(path) }

  s.executables   = ["gitlab-pgrepack"]

  s.homepage      = "https://gitlab.com/gitlab-com/gl-infra/gitlab-pgrepack"
  s.license       = "MIT"

  s.add_runtime_dependency "pg"
  s.add_runtime_dependency "activerecord"
  s.add_runtime_dependency "thor"
  s.add_runtime_dependency "filesize"
  s.add_runtime_dependency "activesupport"
  s.add_runtime_dependency "httparty"
  s.add_runtime_dependency "logger"

  s.add_development_dependency "pry"
  s.add_development_dependency "rspec"
  s.add_development_dependency "rspec-expectations"
end
